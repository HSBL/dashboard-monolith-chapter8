module.exports = {
  home: (req, res) => {
    const locals = {
      contenName: "Statistic",
      layout: "layouts/dashboard",
      data: [{ Post: 10, Visitor: 100, Reader: 10 }],
    };
    res.render("pages/dashboard/home", locals);
  },
  post: require('./post')
};
