const moment = require("moment");
const posts = [
  { id: 1, title: "Lorem", body: "Dolor sit amet", createdAt: Date() },
  { id: 2, title: "Ipsum", body: "Dolor sit amet", createdAt: Date() },
];

module.exports = {
  index: (req, res) => {
    const locals = {
      contentName: "Post",
      data: {
        posts: posts.map((i) => {
          i.fromNow = moment(i.createdAt).fromNow();
          return i;
        }),
      },
    };
    res.render("pages/dashboard/post", locals);
  },
};
