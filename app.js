const express = require("express");
const expressEjsLayouts = require("express-ejs-layouts");
var cookieParser = require("cookie-parser");
const session = require("express-session");

const app = express();
const port = 3000;

app.use(
  session({
    secret: "Buat ini jadi rahasia",
    resave: false,
    saveUninitialized: false,
  })
);
const passport = require("./lib/passport");
app.use(passport.initialize());
app.use(passport.session());

app.use(cookieParser());

app.use(expressEjsLayouts);
app.set("layout", "layouts/default");
// setting view engine
app.use(express.static("public"));
app.set("view engine", "ejs");

// parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// application leverl middleware
const setDefault = (req, res, next) => {
  res.locals.contentName = "Default";
  next();
};

app.use(setDefault);

const router = require("./router");
app.use(router);

// app.get("/", (req, res) => {
//   res.render("index");
// });

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
